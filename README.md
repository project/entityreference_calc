Drupal Entity reference calc module
-----------------------------------

* Maintainer Fredrik Jonsson <http://drupal.org/user/5546>
* Requires - Drupal 7
* License - GPL (see LICENSE)

Overview
--------

This module is heavily based on the Entity Reference Count module.

* <http://drupal.org/project/entityreference_count>

The module provide a field type that can calculate a value (sum/average/min/max)
from a field on a entity that reference the entity the field is on.

Say you put this field on a movie content type and that there are movie reviews
with a rating field that reference movies. Then you can get the average rating
for the movie.

The development of this module was sponsored by Open Technology Fund, a Radio
Free Asia project.

* <https://opentechfund.org/>
* <http://www.rfa.org/>
